package com.atlassian.jira.tests.dependency.plugin.onethousand.impl;

import com.atlassian.jira.tests.dependency.plugin.onethousand.api.OneThousandPluginComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import javax.inject.Named;

@ExportAsService ({OneThousandPluginComponent.class})
@Named ("myPluginComponent")
public class OneThousandPluginComponentImpl implements OneThousandPluginComponent {
}
